# Symfony rabbitmq 
This helps creating connections to RabbitMQ

## Install via composer 
```
composer require fittinq\symfony-rabbitmq
```
## Configure bundle
config/bundles.php

```
<?php

return [
    // ...
    Fittinq\Symfony\RabbitMQ\SymfonyRabbitMQBundle::class => ['all' => true],
];
```

Add the following entries in your .env file and change where needed: 
```
RABBITMQ_HOST=rabbitmq
RABBITMQ_PORT=5672
RABBITMQ_USERNAME=guest
RABBITMQ_PASSWORD=guest
```

Add the following entries in your service.yaml this gives the consumer a handler to handle single messages 
and an error handler if it can't consume any.
```
Fittinq\Symfony\Rabbitmq\Command\ConsumeCommand:
    tags:
        - { name: 'console.command', command: 'name:of:command', description: 'description' }
    arguments:
        - Fittinq\Symfony\RabbitMQ\RabbitMQ\RabbitMQ
        - Fittinq\Symfony\RabbitMQ\Handler\Handler
        - Fittinq\Symfony\RabbitMQ\ErrorHandler\ErrorHandler
        - 'queueName'
```