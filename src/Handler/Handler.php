<?php declare(strict_types=1);

namespace Fittinq\Symfony\RabbitMQ\Handler;

use Fittinq\Symfony\RabbitMQ\ErrorLogging\ErrorLogger;
use Fittinq\Symfony\RabbitMQ\RabbitMQ\HeaderBag;
use PhpAmqpLib\Message\AMQPMessage;
use Psr\Log\LogLevel;
use stdClass;
use Throwable;

abstract class Handler
{
    private string $queue;
    private ErrorLogger $errorLogger;

    public function __construct(string $queue, ErrorLogger $errorLogger)
    {
        $this->queue = $queue;
        $this->errorLogger = $errorLogger;
    }

    public function getQueue(): string
    {
        return $this->queue;
    }

    public function receiveMessage(AMQPMessage $message): void
    {
        try {
            $body = json_decode($message->getBody());

            if ($this->isApplicable($body)) {
                $this->handleMessage(
                    new HeaderBag($message->get_properties()),
                    $body,
                    $this->getExchange($message) ?? '',
                    $message->getRoutingKey() ?? ''
                );
            }

            $message->ack();
        } catch (Throwable $exception) {
            $this->handleException($exception, $message);
            $message->nack();
        }
    }

    protected function isApplicable(stdClass|array $body): bool
    {
        return true;
    }
    public function getExchange(AMQPMessage $message): ?string
    {
        $exchange = $message->getExchange();
        if (empty($exchange)) {
            $routingKey = $message->getRoutingKey();
            $exchange = substr($routingKey, 0, strrpos($routingKey, '.'));
        }

        return $exchange;
    }

    /**
     * @throws Throwable
     */
    abstract public function handleMessage(HeaderBag $headers, stdClass|array $body, string $exchange, string $routingKey);


    protected function handleException(Throwable $exception, AMQPMessage $message): void
    {
        $this->errorLogger->log(
            $this->getLogLevel(),
            $this->formatMessage($exception),
            array_merge(['queue' => $this->queue], $message->get_properties())
        );
    }

    protected function getLogLevel(): string
    {
        return LogLevel::WARNING;
    }

    private function formatMessage(Throwable $e): string
    {
        $message = get_class($e);

        if (!empty($e->getMessage())) {
            $message .= ": {$e->getMessage()}: {$e->getFile()}: {$e->getLine()}";
        }

        return $message;
    }
}
