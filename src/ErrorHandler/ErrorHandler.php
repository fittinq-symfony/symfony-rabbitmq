<?php declare(strict_types=1);

namespace Fittinq\Symfony\RabbitMQ\ErrorHandler;

use Throwable;

interface ErrorHandler
{
    public function handle(Throwable $e, string $queue): void;
}
