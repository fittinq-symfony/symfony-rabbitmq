<?php declare(strict_types=1);

namespace Fittinq\Symfony\RabbitMQ\Command;

use Fittinq\Symfony\RabbitMQ\ErrorHandler\ErrorHandler;
use Fittinq\Symfony\RabbitMQ\Handler\Handler;
use Fittinq\Symfony\RabbitMQ\RabbitMQ\RabbitMQ;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Throwable;

class ConsumeCommand extends Command
{
    private RabbitMQ $rabbitMQ;
    private Handler $handler;
    private ErrorHandler $errorHandler;
    private string $queueName;

    public function __construct(RabbitMQ $rabbitMQ, Handler $handler, ErrorHandler $errorHandler)
    {
        parent::__construct("default");

        $this->rabbitMQ = $rabbitMQ;
        $this->handler = $handler;
        $this->errorHandler = $errorHandler;
        $this->queueName = $handler->getQueue();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        try {
            $queue = $this->rabbitMQ->getQueue($this->queueName);
            $queue->consume($this->handler);
            return Command::SUCCESS;
        } catch (Throwable $e) {
            $this->errorHandler->handle($e, $this->queueName);
            return Command::FAILURE;
        }
    }
}
