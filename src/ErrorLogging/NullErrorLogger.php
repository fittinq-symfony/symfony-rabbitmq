<?php declare(strict_types=1);

namespace Fittinq\Symfony\RabbitMQ\ErrorLogging;

use Psr\Log\NullLogger;

class NullErrorLogger extends ErrorLogger
{
    public function __construct()
    {
        parent::__construct(new NullLogger());
    }
}
