<?php declare(strict_types=1);

namespace Fittinq\Symfony\RabbitMQ\ErrorLogging;

use Psr\Log\AbstractLogger;
use Psr\Log\LoggerInterface;
use Stringable;

class ErrorLogger extends AbstractLogger
{
    private LoggerInterface $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function log($level, Stringable|string $message, array $context = []): void
    {
        $this->logger->log($level, $message, $context);
    }
}
