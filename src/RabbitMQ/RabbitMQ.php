<?php declare(strict_types=1);

namespace Fittinq\Symfony\RabbitMQ\RabbitMQ;

use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AMQPLazyConnection;

class RabbitMQ
{
    private AMQPLazyConnection $connection;
    private AMQPChannel $channel;

    /**
     * @var Exchange[]
     */
    private array $exchanges = [];

    /**
     * @var Queue[]
     */
    private array $queue = [];

    public function __construct(AMQPLazyConnection $connection, string $connectionName = '')
    {
        $this->connection = $connection;

        AMQPLazyConnection::$LIBRARY_PROPERTIES['connection_name'] = ['S', $connectionName];
    }

    public function hasExchange(string $name): bool
    {
        return isset($this->exchanges[$name]);
    }

    public function hasQueue(string $name): bool
    {
        return isset($this->queues[$name]);
    }

    public function getExchange(string $name): Exchange
    {
        if (!$this->connection->isConnected()) {
            $this->channel = $this->connection->channel();
        }

        if (!$this->hasExchange($name)) {
            $exchange = new Exchange($this->channel, $name);
            $this->exchanges[$name] = $exchange;
        }

        return $this->exchanges[$name];
    }

    public function getQueue(string $name): Queue
    {
        if (!$this->connection->isConnected()) {
            $this->channel = $this->connection->channel();
        }

        if (!$this->hasQueue($name)) {
            $queue = new Queue($this->connection, $this->channel, $name);
            $this->queue[$name] = $queue;
        }

        return $this->queue[$name];
    }
}
