<?php declare(strict_types=1);

namespace Fittinq\Symfony\RabbitMQ\RabbitMQ;

use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Message\AMQPMessage;
use stdClass;

class Exchange
{
    private AMQPChannel $channel;
    private string $name;

    public function __construct(AMQPChannel $channel, string $name)
    {
        $this->channel = $channel;
        $this->name = $name;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function declare(string $type): void
    {
        $this->channel->exchange_declare($this->name, $type, false, true, false);
    }

    public function produce(stdClass|array $body, string $routingKey = ''): void {
        $this->createAndPublishMessage($body, uniqid(), $routingKey);
    }

    public function produceWithId(stdClass|array $body, string $messageId, string $routingKey = ''): void {
        $this->createAndPublishMessage($body, $messageId, $routingKey);
    }

    private function createAndPublishMessage(stdClass|array $body, string $messageId, string $routingKey): void {
        $message = new AMQPMessage(json_encode($body));
        $message->set('message_id', $messageId);
        $message->set('timestamp', round(microtime(true) * 1000));

        $this->channel->basic_publish($message, $this->name, $routingKey);
    }

    public function delete(): void
    {
        $this->channel->exchange_delete($this->name);
    }
}
