<?php declare(strict_types=1);

namespace Fittinq\Symfony\RabbitMQ\RabbitMQ;

use Fittinq\Symfony\RabbitMQ\Handler\Handler;
use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AMQPLazyConnection;
use PhpAmqpLib\Wire\AMQPTable;
use Test\Fittinq\Symfony\RabbitMQ\Exceptions\TestMessagesDepletedException;
use Throwable;

class Queue
{
    private AMQPLazyConnection $connection;
    private AMQPChannel $channel;
    private string $name;

    public function __construct(AMQPLazyConnection $connection, AMQPChannel $channel, string $name)
    {
        $this->connection = $connection;
        $this->channel = $channel;
        $this->name = $name;
    }

    public function bind(Exchange $exchange, string $routingKey = ''): void
    {
        $this->channel->queue_bind($this->name, $exchange->getName(), $routingKey);
    }

    public function unbind(Exchange $exchange, string $routingKey = ''): void
    {
        $this->channel->queue_unbind($this->name, $exchange->getName(), $routingKey);
    }

    public function declare(): void
    {
        $this->channel->queue_declare($this->name, false, true, false, false, false, new AMQPTable(['x-queue-type' => 'quorum']));
    }

    /**
     * @throws Throwable
     */
    public function consume(Handler $handler, int $messageLimit = 0): void
    {
        try {
            $consumedMessages = 0;
            $this->channel->basic_qos(0, 1, false);
            $this->channel->basic_consume($this->name, '', false, false, false, false, [$handler, 'receiveMessage']);

            while ($this->channel->is_consuming() && ($consumedMessages < $messageLimit || $messageLimit == 0)) {
                $this->channel->wait();
                $consumedMessages++;
            }

            $this->connection->close();

        } catch (TestMessagesDepletedException) {
            /**
             * Note that this is only used to prevent infinite loops in testing. Normally the queues without message limits will
             * run forever. However, this breaks lots of tests since they keep looping forever as well.
             */
            return;
        } catch (Throwable $e) {
            $this->connection->close();
            throw $e;
        }
    }

    public function delete(): void
    {
        $this->channel->queue_delete($this->name);
    }
}
