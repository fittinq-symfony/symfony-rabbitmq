<?php declare(strict_types=1);

namespace Fittinq\Symfony\RabbitMQ\RabbitMQ;

class HeaderBag
{
    private array $headers = [];

    public function __construct(array $headers = [])
    {
        $this->headers = $headers;
    }

    public function get(string $name, mixed $default = null): mixed {
        return $this->headers[$name] ?? $default;
    }
}
