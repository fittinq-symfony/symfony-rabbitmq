<?php declare(strict_types=1);

namespace Test\Fittinq\Symfony\RabbitMQ\Exceptions;

use Exception;

class TestMessagesDepletedException extends Exception
{
    /**
     * Note that this is only used to prevent infinite loops in testing. Normally the queues without message limits will
     * run forever. However, this breaks lots of tests since they keep looping forever as well.
     */
}