<?php declare(strict_types=1);

namespace Test\Fittinq\Symfony\RabbitMQ\Command;

use Fittinq\Symfony\RabbitMQ\Command\ConsumeCommand;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\HttpKernel\KernelInterface;
use Exception;

class Configuration extends \Test\Fittinq\Symfony\RabbitMQ\Configuration
{
    private Application $application;
    private ConsumeCommand $command;

    /**
     * @throws Exception
     */
    public function __construct(KernelInterface $kernel)
    {
        parent::__construct();
        $this->application = new Application($kernel);
        $this->setupCommand();
    }

    public function configure(): Command
    {
        return $this->command;
    }

    public function setupCommand(): void
    {
        $this->command = new ConsumeCommand($this->getRabbitMQ(), $this->getHandlerMock(), $this->getErrorHandlerMock(), 'source.event.target');
        $this->application->add($this->command);
    }
}
