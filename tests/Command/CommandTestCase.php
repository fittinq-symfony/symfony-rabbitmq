<?php declare(strict_types=1);

namespace Test\Fittinq\Symfony\RabbitMQ\Command;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;

class CommandTestCase extends KernelTestCase
{
    protected Configuration $configuration;

    protected function setUp(): void
    {
        parent::setUp();
        static::bootKernel();
        $this->configuration = new Configuration(static::$kernel);
    }

    protected function runCommand(): CommandTester
    {
        $commandTester = new CommandTester($this->configuration->configure());
        $commandTester->execute([]);

        return $commandTester;
    }
}
