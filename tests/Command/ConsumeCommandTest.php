<?php declare(strict_types=1);

namespace Test\Fittinq\Symfony\RabbitMQ\Command;

use Fittinq\Symfony\RabbitMQ\RabbitMQ\HeaderBag;
use PhpAmqpLib\Exception\AMQPIOException;
use Symfony\Component\Console\Command\Command;
use Test\Fittinq\Symfony\RabbitMQ\Consumer\ErrorHandlerMock;
use Test\Fittinq\Symfony\RabbitMQ\Consumer\HandlerMock;
use Test\Fittinq\Symfony\RabbitMQ\RabbitMQ\AMQPChannelMock;
use Test\Fittinq\Symfony\RabbitMQ\RabbitMQ\AMQPLazyConnectionMock;
use stdClass;

class ConsumeCommandTest extends CommandTestCase
{
    private AMQPLazyConnectionMock $connection;
    private ErrorHandlerMock $errorHandler;
    private HandlerMock $handler;
    private AMQPChannelMock $channel;

    protected function setUp(): void
    {
        parent::setUp();

        $this->connection = $this->configuration->getConnection();
        $this->errorHandler = $this->configuration->getErrorHandlerMock();
        $this->channel = $this->configuration->getChannel();
        $this->handler = $this->configuration->getHandlerMock();
    }

    public function test_consumeCommandReturnsExecuteOnSuccess(): void
    {
        $exitCode = $this->runCommand();
        $this->assertEquals(Command::SUCCESS, $exitCode->getStatusCode());
    }

    public function test_whenConsumingFailsExecuteErrorHandler(): void
    {
        $exception = new AMQPIOException('failure');
        $message = new stdClass();
        $message->consume = 'command';
        $headerBag = new HeaderBag([
            'message_id' => '87234878',
            'timestamp' => '12398498',
        ]);
        $this->connection->setAMQPIOException($exception);
        $this->channel->sendMessageToQueue($headerBag, 'source.event.target', $message);

        $this->runCommand();

        $this->errorHandler->assertExceptionWasThrownForQueue($exception, 'source.event.target');
    }

    public function test_handlerReceivesMessageWhenConsumeCommandIsCalled(): void
    {
        $message = new stdClass();
        $message->vehicle = 'bicycle';

        $headerBag = new HeaderBag([
            'message_id' => '87234878',
            'timestamp' => '12398498',
        ]);

        $this->channel->sendMessageToQueue($headerBag, 'source.event.target', $message, '', 'source.event.target');
        $this->runCommand();

        $this->handler->expectHandlerToHaveReceivedMessage($headerBag,  $message, 'source.event', 'source.event.target');
    }
}
