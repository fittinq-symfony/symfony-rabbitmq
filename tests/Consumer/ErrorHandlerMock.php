<?php declare(strict_types=1);

namespace Test\Fittinq\Symfony\RabbitMQ\Consumer;

use Fittinq\Symfony\RabbitMQ\ErrorHandler\ErrorHandler;
use PHPUnit\Framework\Assert;
use Throwable;

class ErrorHandlerMock implements ErrorHandler
{
    private ?Throwable $handledError = null;
    private ?string $handledQueue = null;

    public function handle(Throwable $e, string $queue): void
    {
        $this->handledError = $e;
        $this->handledQueue = $queue;
    }

    public function assertExceptionWasThrownForQueue(Throwable $e, string $queue): void
    {
        Assert::assertEquals($e, $this->handledError);
        Assert::assertEquals($queue, $this->handledQueue);
    }
}
