<?php declare(strict_types=1);

namespace Test\Fittinq\Symfony\RabbitMQ\Consumer;

use Exception;
use Fittinq\Symfony\RabbitMQ\RabbitMQ\HeaderBag;
use Fittinq\Symfony\RabbitMQ\RabbitMQ\RabbitMQ;
use PHPUnit\Framework\TestCase;
use stdClass;
use Test\Fittinq\Symfony\RabbitMQ\Configuration;
use Throwable;

class ConsumerTest extends TestCase
{
    private Configuration $configuration;

    /**
     * @throws Exception|Throwable
     */
    public function test_consumedStdClassMessagesArePassedToTheHandler(): void
    {
        $exchangeName = 'greetings';
        $routingKey = 'to_the_left';

        $rabbitmq = new RabbitMQ($this->configuration->getConnection());
        $queue = $rabbitmq->getQueue('hello');

        $greetings = [
            $this->createGreeting("bonjour"),
            $this->createGreeting("hello"),
            $this->createGreeting("hallo"),
            $this->createGreeting("gutentag"),
            $this->createGreeting("merhaba"),
        ];

        $headerBag = new HeaderBag([
            'message_id' => '123456',
            'timestamp' => '16585987984',
        ]);

        foreach ($greetings as $greeting) {
            $this->configuration->getChannel()->sendMessageToQueue(
                $headerBag,
                'hello',
                $greeting,
                $exchangeName,
                $routingKey
            );
        }

        $queue->consume($this->configuration->getHandlerMock());

        foreach ($greetings as $greeting) {
            $this->configuration->getHandlerMock()->expectHandlerToHaveReceivedMessage($headerBag, $greeting, $exchangeName, $routingKey);
        }
    }

    /**
     * @throws Throwable
     */
    public function test_consumedMessagesAreNotPassedToTheHandlerWhenNotApplicable(): void
    {
        $exchangeName = 'greetings';
        $routingKey = 'to_the_left';

        $rabbitmq = new RabbitMQ($this->configuration->getConnection());
        $queue = $rabbitmq->getQueue('hello');

        $headerBag = new HeaderBag([
            'message_id' => '123456',
            'timestamp' => '16585987984',
        ]);

        $greeting = $this->createGreeting("Ignore");

        $this->configuration->getChannel()->sendMessageToQueue(
            $headerBag,
            'hello',
            $greeting,
            $exchangeName,
            $routingKey
        );

        $queue->consume($this->configuration->getHandlerMock());
        $this->configuration->getHandlerMock()->expectHandlerToNotHaveReceivedAnyMessage();
    }

    private function createGreeting(string $message): stdClass
    {
        $greeting = new stdClass();
        $greeting->greeting = $message;

        return $greeting;
    }

    /**
     * @throws Exception|Throwable
     */
    public function test_consumedArrayMessagesArePassedToTheHandler(): void
    {
        $exchangeName = 'goodbyes';
        $routingKey = 'to_the_right';

        $rabbitmq = new RabbitMQ($this->configuration->getConnection());
        $queue = $rabbitmq->getQueue('goodbye');

        $greetings = [
            $this->createGreeting("Au revoir"),
            $this->createGreeting("goodbye"),
            $this->createGreeting("vaarwel"),
            $this->createGreeting("Auf Wiedersehen"),
            $this->createGreeting("hoşçakal"),
        ];

        $headerBag = new HeaderBag([
            'message_id' => '78910',
            'timestamp' => '16585987984',
        ]);

        $this->configuration->getChannel()->sendMessageToQueue(
            $headerBag,
            'goodbye',
            $greetings,
            $exchangeName,
            $routingKey
        );

        $queue->consume($this->configuration->getHandlerMock());

        $this->configuration->getHandlerMock()->expectHandlerToHaveReceivedMessage($headerBag, $greetings, $exchangeName, $routingKey);
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->configuration = new Configuration();
    }
}

