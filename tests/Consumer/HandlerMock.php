<?php declare(strict_types=1);

namespace Test\Fittinq\Symfony\RabbitMQ\Consumer;

use Fittinq\Symfony\RabbitMQ\Handler\Handler;
use Fittinq\Symfony\RabbitMQ\RabbitMQ\HeaderBag;
use PHPUnit\Framework\Assert;
use stdClass;
use Throwable;

class HandlerMock extends Handler
{
    private ?Throwable $throwException = null;
    private array $messages = [];

    public function setThrowException(Throwable $throwException): void
    {
        $this->throwException = $throwException;
    }

    public function handleMessage(HeaderBag $headers, stdClass|array $body, string $exchange, string $routingKey): void
    {
        $message = new stdClass();
        $message->exchange = $exchange;
        $message->routingKey = $routingKey;
        $message->headers = $headers;
        $message->body = $body;

        $this->messages[] = $message;

        if ($this->throwException) {
            throw $this->throwException;
        }
    }
    protected function isApplicable(array|stdClass $body): bool
    {
        if (isset($body->greeting) && $body->greeting === 'Ignore'){
            return false;
        }

        return true;
    }

    public function expectHandlerToHaveReceivedMessage(HeaderBag $headers, stdClass|array $body, string $exchange, string $routingKey): void
    {
        $receivedMessage = false;

        foreach ($this->messages as $message) {
            if (
                $message->exchange === $exchange &&
                $message->routingKey === $routingKey &&
                $message->headers->get('message_id') == $headers->get('message_id') &&
                $message->headers->get('timestamp') == $headers->get('timestamp') &&
                $message->body == $body
            ) {
                $receivedMessage = true;
            }
        }

        Assert::assertTrue($receivedMessage);
    }

    public function expectHandlerToNotHaveReceivedAnyMessage(): void
    {
        Assert::assertEmpty($this->messages);
    }
}
