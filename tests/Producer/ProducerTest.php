<?php declare(strict_types=1);

namespace Test\Fittinq\Symfony\RabbitMQ\Producer;

use Fittinq\Symfony\RabbitMQ\RabbitMQ\RabbitMQ;
use PHPUnit\Framework\TestCase;
use stdClass;
use Test\Fittinq\Symfony\RabbitMQ\Configuration;

class ProducerTest extends  TestCase
{
    private Configuration $configuration;

    protected function setUp(): void
    {
        parent::setUp();
        $this->configuration = new Configuration();
    }

    public function test_publishMessageToExchange(): void
    {
        $rabbitmq = new RabbitMQ($this->configuration->getConnection());
        $message = new stdClass();
        $message->hello = 'world';

        $exchange = $rabbitmq->getExchange('greetings');
        $exchange->produce($message, 'welcome');

        $this->configuration->getChannel()->expectMessageToBeSentToExchange('greetings', $message, 'welcome');
    }

    public function test_publishedMessagesShouldHaveUniqueId(): void
    {
        $rabbitmq = new RabbitMQ($this->configuration->getConnection());
        $message = new stdClass();
        $message->hello = 'world';

        $exchange = $rabbitmq->getExchange('greetings');
        $exchange->produce($message, 'route66');

        $this->configuration->getChannel()->expectMessageToHaveUniqueId();
    }

    public function test_publishedMessagesShouldHaveTimestamp(): void
    {
        $rabbitmq = new RabbitMQ($this->configuration->getConnection());
        $message = new stdClass();
        $message->hello = 'world';

        $exchange = $rabbitmq->getExchange('greetings');
        $exchange->produce($message, 'route66');

        $this->configuration->getChannel()->expectMessageToHaveTimestamp();
    }
}
