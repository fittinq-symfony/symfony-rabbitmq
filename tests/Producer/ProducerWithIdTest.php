<?php declare(strict_types=1);

namespace Test\Fittinq\Symfony\RabbitMQ\Producer;

use Fittinq\Symfony\RabbitMQ\RabbitMQ\RabbitMQ;
use PHPUnit\Framework\TestCase;
use stdClass;
use Test\Fittinq\Symfony\RabbitMQ\Configuration;

class ProducerWithIdTest extends  TestCase
{
    private Configuration $configuration;

    protected function setUp(): void
    {
        parent::setUp();
        $this->configuration = new Configuration();
    }

    public function test_publishMessageToExchange(): void
    {
        $rabbitmq = new RabbitMQ($this->configuration->getConnection());
        $message = new stdClass();
        $message->hello = 'world';

        $exchange = $rabbitmq->getExchange('greetings');
        $exchange->produceWithId($message, 'test234', 'welcome');

        $this->configuration->getChannel()->expectMessageToBeSentToExchange('greetings', $message, 'welcome');
    }

    public function test_publishedMessagesShouldHaveUniqueId(): void
    {
        $id = 'ade453';
        $rabbitmq = new RabbitMQ($this->configuration->getConnection());
        $message = new stdClass();
        $message->hello = 'world';

        $exchange = $rabbitmq->getExchange('greetings');
        $exchange->produceWithId($message, $id, 'route66');

        $this->configuration->getChannel()->expectMessageToHaveExpectedId($id);
    }

    public function test_publishedMessagesShouldHaveTimestamp(): void
    {
        $rabbitmq = new RabbitMQ($this->configuration->getConnection());
        $message = new stdClass();
        $message->hello = 'world';

        $exchange = $rabbitmq->getExchange('greetings');
        $exchange->produceWithId($message, 'test123', 'route66');

        $this->configuration->getChannel()->expectMessageToHaveTimestamp();
    }
}
