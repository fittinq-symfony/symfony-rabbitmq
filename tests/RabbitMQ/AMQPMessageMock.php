<?php declare(strict_types=1);

namespace Test\Fittinq\Symfony\RabbitMQ\RabbitMQ;

use PhpAmqpLib\Message\AMQPMessage;
use PhpAmqpLib\Wire\AMQPTable;
use PHPUnit\Framework\Assert;
use Throwable;

class AMQPMessageMock extends AMQPMessage
{
    private bool $ack = false;
    private bool $nack = false;
    private bool $rejected = false;
    private bool $requeue = false;
    private bool $multiple = false;

    public function __construct($body = '', $properties = array())
    {
        parent::__construct($body, $properties);
    }

    public function ack($multiple = false): void
    {
        $this->ack = true;
        $this->multiple = $multiple;
    }

    public function nack($requeue = false, $multiple = false): void
    {
        $this->nack = true;
        $this->requeue = $requeue;
        $this->multiple = $multiple;
    }

    /**
     * We never use multiple and simply acknowledge message one by one.
     */
    public function expectMessageToBeAcknowledged(): void
    {
        Assert::assertTrue($this->ack);
        Assert::assertFalse($this->nack);
        Assert::assertFalse($this->rejected);
        Assert::assertFalse($this->multiple);
    }

    /**
     * We never use multiple and simply dead letter message one by one.
     */
    public function expectMessageToBeDeadLettered(): void
    {
        Assert::assertFalse($this->ack);
        Assert::assertTrue($this->nack);
        Assert::assertFalse($this->requeue);
        Assert::assertFalse($this->rejected);
        Assert::assertFalse($this->multiple);
    }
}
