<?php declare(strict_types=1);

namespace Test\Fittinq\Symfony\RabbitMQ\RabbitMQ;

use PhpAmqpLib\Channel\AbstractChannel;
use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AMQPLazyConnection;
use PhpAmqpLib\Exception\AMQPIOException;
use PHPUnit\Framework\Assert;

class AMQPLazyConnectionMock extends AMQPLazyConnection
{
    private array $createdChannels = [];
    private AMQPChannelMock $channel;
    private bool $isConnected = false;
    private bool $isClosed = false;
    private ?AMQPIOException $amqpIOException = null;

    /** @noinspection PhpMissingParentConstructorInspection */
    public function __construct(AMQPChannelMock $channel)
    {
        $this->channel = $channel;
    }

    /** @noinspection PhpUnused */
    public function setAMQPIOException(AMQPIOException $exception): void
    {
        $this->amqpIOException = $exception;
    }

    /**
     * @throws AMQPIOException
     */
    public function channel($channel_id = null): AMQPChannel|AMQPChannelMock|AbstractChannel
    {
        if ($this->amqpIOException) {
            throw $this->amqpIOException;
        }

        $channel_id = $channel_id ?: PHP_INT_MAX;
        $this->createdChannels[] = $channel_id;
        $this->isConnected = true;
        return $this->channel;
    }

    public function close($reply_code = 0, $reply_text = '', $method_sig = array(0, 0)): void
    {
        $this->isClosed = true;
    }

    public function isConnected(): bool
    {
        return $this->isConnected;
    }

    public function expectOnlyOneChannelToBeOpened(): void
    {
        Assert::assertCount(1, $this->createdChannels);
    }

    public function expectToBeClosed(): void
    {
        Assert::assertTrue($this->isClosed);
    }
}
