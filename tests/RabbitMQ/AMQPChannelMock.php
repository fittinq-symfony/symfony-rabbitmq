<?php declare(strict_types=1);

namespace Test\Fittinq\Symfony\RabbitMQ\RabbitMQ;

use Exception;
use Fittinq\Symfony\RabbitMQ\RabbitMQ\HeaderBag;
use PhpAmqpLib\Channel\AMQPChannel;
use PHPUnit\Framework\Assert;
use stdClass;
use Test\Fittinq\Symfony\RabbitMQ\Exceptions\TestMessagesDepletedException;

class AMQPChannelMock extends AMQPChannel
{
    private array $exchanges = [];
    private array $queues = [];
    private array $binds = [];

    // producing
    private array $producingMessages = [];

    // consuming
    private array $consumingMessages = [];
    private string $currentQueue;
    private mixed $currentCallback;
    private bool $isConsuming = false;
    private ?Exception $exception = null;

    /** @noinspection PhpMissingParentConstructorInspection */
    public function __construct()
    {
    }

    public function setException(Exception $exception): void
    {
        $this->exception = $exception;
    }

    public function exchange_declare($exchange, $type, $passive = false, $durable = false, $auto_delete = true, $internal = false, $nowait = false, $arguments = array(), $ticket = null): void
    {
        $this->exchanges[$exchange] = new stdClass();
        $this->exchanges[$exchange]->name = $exchange;
        $this->exchanges[$exchange]->type = $type;
        $this->exchanges[$exchange]->passive = $passive;
        $this->exchanges[$exchange]->durable = $durable;
        $this->exchanges[$exchange]->auto_delete = $auto_delete;
        $this->exchanges[$exchange]->deleted = false;
    }

    public function exchange_delete($exchange, $if_unused = false, $nowait = false, $ticket = null): void
    {
        $this->exchanges[$exchange]->deleted = true;
    }

    public function queue_declare($queue = '', $passive = false, $durable = false, $exclusive = false, $auto_delete = true, $nowait = false, $arguments = array(), $ticket = null): void
    {
        $this->queues[$queue] = new stdClass();
        $this->queues[$queue]->name = $queue;
        $this->queues[$queue]->passive = $passive;
        $this->queues[$queue]->durable = $durable;
        $this->queues[$queue]->exclusive = $exclusive;
        $this->queues[$queue]->auto_delete = $auto_delete;
        $this->queues[$queue]->deleted = false;
        $this->queues[$queue]->queueType = false;
        $this->queues[$queue]->arguments = $arguments;
    }

    public function queue_delete($queue = '', $if_unused = false, $if_empty = false, $nowait = false, $ticket = null): void
    {
        $this->queues[$queue]->deleted = true;
    }

    public function queue_bind($queue, $exchange, $routing_key = '', $nowait = false, $arguments = array(), $ticket = null): void
    {
        $key = "{$queue}.{$exchange}.{$routing_key}";
        $this->binds[$key] = new stdClass();
        $this->binds[$key]->queue = $queue;
        $this->binds[$key]->exchange = $exchange;
        $this->binds[$key]->routing_key = $routing_key;
    }

    public function queue_unbind($queue, $exchange, $routing_key = '', $arguments = array(), $ticket = null): void
    {
        $key = "{$queue}.{$exchange}.{$routing_key}";
        unset($this->binds[$key]);
    }

    public function basic_publish($msg, $exchange = '', $routing_key = '', $mandatory = false, $immediate = false, $ticket = null): void
    {
        $message = new stdClass();
        $message->exchange = $exchange;
        $message->routingKey = $routing_key;
        $message->message = $msg;

        $this->producingMessages[] = $message;
    }

    public function basic_qos($prefetch_size, $prefetch_count, $a_global) {

    }

    public function basic_consume($queue = '', $consumer_tag = '', $no_local = false, $no_ack = false, $exclusive = false, $nowait = false, $callback = null, $ticket = null, $arguments = array()): void
    {
        $this->isConsuming = true;
        $this->currentQueue = $queue;
        $this->currentCallback = $callback;
    }

    public function is_consuming(): bool
    {
        return $this->isConsuming;
    }

    public function sendMessageToQueue(HeaderBag $headers, string $queue, stdClass|array $body, $exchange = '', $routingKey = ''): AMQPMessageMock
    {
        if (!array_key_exists($queue, $this->consumingMessages)) {
            $this->consumingMessages[$queue] = [];
        }

        $message = new AMQPMessageMock(json_encode($body));
        $message->setDeliveryInfo("", false, $exchange, $routingKey);
        $message->set('message_id', $headers->get('message_id', uniqid()));
        $message->set('application_headers', $headers->get('application_headers'));
        $message->set('timestamp', $headers->get('timestamp', time()));
        $this->consumingMessages[$queue][] = $message;

        return $message;
    }

    /**
     * @throws TestMessagesDepletedException
     * @throws Exception
     */
    public function wait($allowed_methods = null, $non_blocking = false, $timeout = 0): void
    {
        $message = null;

        if ($this->exception) {
            throw $this->exception;
        }

        if (isset($this->consumingMessages[$this->currentQueue])) {
            $message = array_pop($this->consumingMessages[$this->currentQueue]);
        }

        if ($message) {
            call_user_func($this->currentCallback, $message);
        } else {
            throw new TestMessagesDepletedException();
        }
    }

    public function expectMessageToBeSentToExchange(string $exchange, stdClass $body, string $routingKey, int $index = 0): void
    {
        Assert::assertEquals($body, json_decode($this->producingMessages[$index]->message->getBody()));
        Assert::assertEquals($exchange, $this->producingMessages[$index]->exchange);
        Assert::assertEquals($routingKey, $this->producingMessages[$index]->routingKey);
    }

    public function expectMessageToBeSentToExchangeWithMessageId(string $exchange, stdClass $body, string $messageId, string $routingKey, int $index = 0): void
    {
        Assert::assertEquals($body, json_decode($this->producingMessages[$index]->message->getBody()));
        Assert::assertEquals($exchange, $this->producingMessages[$index]->exchange);
        Assert::assertEquals($messageId, $this->producingMessages[$index]->message->get('message_id'));
        Assert::assertEquals($routingKey, $this->producingMessages[$index]->routingKey);
    }

    public function expectNoMessageToBeSentToExchange(string $exchange): void
    {
        foreach ($this->producingMessages as $message) {
            Assert::assertNotEquals($exchange, $message->exchange, 'Expected no message to be sent to exchange');
        }

        /* This is here because the producingMessages array could be empty, and tests could become risky if there are no
         * asserts. This makes sure that there is at least one assert.
         */
        Assert::assertTrue(true);
    }

    public function expectQueueToBeDeclared(string $queue): void
    {
        Assert::assertArrayHasKey($queue, $this->queues);
    }

    public function expectQueueToBeBoundToExchange(string $exchange, string $queue, string $routingKey): void
    {
        Assert::assertArrayHasKey("{$queue}.{$exchange}.{$routingKey}", $this->binds);
    }

    public function expectQueueNotToBeBoundToExchange(string $exchange, string $queue, string $routingKey): void
    {
        Assert::assertArrayNotHasKey("{$queue}.{$exchange}.{$routingKey}", $this->binds);
    }

    public function expectQueueToBeDurable(string $queue): void
    {
        Assert::assertTrue($this->queues[$queue]->durable);
    }

    public function expectQueueToBeDeleted(string $queue): void
    {
        Assert::assertTrue($this->queues[$queue]->deleted);
    }

    public function expectQueueToBeOfTypeQuorum(string $queue): void
    {
        Assert::assertEquals('quorum', $this->queues[$queue]->arguments['x-queue-type']);
    }

    public function expectQueueNotFound(string $queue): void
    {
        Assert::assertArrayNotHasKey($queue, $this->queues);
    }

    public function expectExchangeToBeDeclared(string $exchange, string $type): void
    {
        Assert::assertArrayHasKey($exchange, $this->exchanges);
        Assert::assertEquals($this->exchanges[$exchange]->type, $type);
    }

    public function expectExchangeToBeDurable(string $exchange): void
    {
        Assert::assertTrue($this->exchanges[$exchange]->durable);
    }

    public function expectExchangeToBeDeleted(string $exchange): void
    {
        Assert::assertTrue($this->exchanges[$exchange]->deleted);
    }

    public function expectExchangeNotFound(string $exchange): void
    {
        Assert::assertArrayNotHasKey($exchange, $this->exchanges);
    }

    public function expectMessageToHaveUniqueId(int $index = 0): void
    {
        Assert::assertMatchesRegularExpression('/[A-Fa-f0-9]{13}/', $this->producingMessages[$index]->message->get('message_id'));
    }

    public function expectMessageToHaveExpectedId(string $messageId, int $index = 0): void
    {
        Assert::assertEquals($messageId, $this->producingMessages[$index]->message->get('message_id'));
    }

    public function expectMessageToHaveTimestamp(int $index = 0): void
    {
        $now = round(microtime(true) * 1000);
        Assert::assertLessThan(1000, abs($now - $this->producingMessages[$index]->message->get('timestamp')));
    }

    public function hasMessagesInQueue(int $expected, string $queue): void
    {
        Assert::assertCount($expected, $this->consumingMessages[$queue]);
    }
}
