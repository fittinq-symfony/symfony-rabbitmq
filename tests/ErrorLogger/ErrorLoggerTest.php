<?php declare(strict_types=1);

namespace Test\Fittinq\Symfony\RabbitMQ\ErrorLogger;

use Fittinq\Symfony\RabbitMQ\ErrorLogging\ErrorLogger;
use PHPUnit\Framework\TestCase;
use Psr\Log\LogLevel;
use Test\Fittinq\Symfony\Mock\LoggerInterface\LoggerInterfaceMock;

class ErrorLoggerTest extends TestCase
{
    public function test_errorLoggerTest(): void
    {
        $loggerInterface = new LoggerInterfaceMock();
        $errorLogger = new ErrorLogger($loggerInterface);

        $errorLogger->log(LogLevel::ERROR, 'error');
        $loggerInterface->assertMessageHasBeenLogged('error');
    }
}
