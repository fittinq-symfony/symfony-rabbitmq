<?php declare(strict_types=1);

namespace Test\Fittinq\Symfony\RabbitMQ\ErrorLogger;

use Fittinq\Symfony\RabbitMQ\ErrorLogging\NullErrorLogger;
use PHPUnit\Framework\TestCase;

class NullErrorLoggerTest extends TestCase
{
    public function test_nullErrorLoggerTest(): void
    {
        new NullErrorLogger();

        $this->assertTrue(true);
    }
}
