<?php declare(strict_types=1);

namespace Test\Fittinq\Symfony\RabbitMQ\Exchange;

use PhpAmqpLib\Exchange\AMQPExchangeType;
use PHPUnit\Framework\TestCase;
use Test\Fittinq\Symfony\RabbitMQ\Configuration;

class ExchangeTest extends TestCase
{
    private Configuration $configuration;

    protected function setUp(): void
    {
        parent::setUp();
        $this->configuration = new Configuration();
    }

    public function test_exchangesShouldBeDurable(): void
    {
        $exchange = $this->configuration->getRabbitMQ()->getExchange('fruits');
        $exchange->declare(AMQPExchangeType::FANOUT);

        $this->configuration->getChannel()->expectExchangeToBeDurable('fruits');
    }


    public function test_deleteExchangeIfRequested(): void
    {
        $exchange = $this->configuration->getRabbitMQ()->getExchange('fruits');
        $exchange->declare(AMQPExchangeType::FANOUT);

        $exchange->delete();

        $this->configuration->getChannel()->expectExchangeToBeDeleted('fruits');
    }
}
