<?php declare(strict_types=1);

namespace Test\Fittinq\Symfony\RabbitMQ\Queue;

use PHPUnit\Framework\TestCase;
use Test\Fittinq\Symfony\RabbitMQ\Configuration;

class BindTest extends  TestCase
{
    private Configuration $configuration;

    protected function setUp(): void
    {
        parent::setUp();
        $this->configuration = new Configuration();
    }

    public function test_multipleQueuesCanBeBoundToExchange(): void
    {
        $bananasQueue = $this->configuration->getRabbitMQ()->getQueue('bananas');
        $applesQueue = $this->configuration->getRabbitMQ()->getQueue('apples');
        $fruitsExchange = $this->configuration->getRabbitMQ()->getExchange('fruits');

        $bananasQueue->bind($fruitsExchange, "chiquita");
        $applesQueue->bind($fruitsExchange, "elstar");

        $this->configuration->getChannel()->expectQueueToBeBoundToExchange('fruits', "bananas", "chiquita");
        $this->configuration->getChannel()->expectQueueToBeBoundToExchange('fruits', "apples", "elstar");
    }

    public function test_queueBindingAndUnbindingToExchange(): void
    {
        $carrotsQueue = $this->configuration->getRabbitMQ()->getQueue('carrots');
        $vegetablesExchange = $this->configuration->getRabbitMQ()->getExchange('vegetables');

        $carrotsQueue->bind($vegetablesExchange, "carafresh");

        $this->configuration->getChannel()->expectQueueToBeBoundToExchange('vegetables', "carrots", "carafresh");

        $carrotsQueue->unbind($vegetablesExchange, "carafresh");
        $this->configuration->getChannel()->expectQueueNotToBeBoundToExchange('vegetables', "carrots", "carafresh");

    }
}