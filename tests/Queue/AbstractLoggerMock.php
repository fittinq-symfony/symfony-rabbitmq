<?php declare(strict_types=1);

namespace Test\Fittinq\Symfony\RabbitMQ\Queue;

use Fittinq\Symfony\RabbitMQ\ErrorLogging\ErrorLogger;
use PHPUnit\Framework\Assert;
use Stringable;

class AbstractLoggerMock extends ErrorLogger
{
    private string $level;
    private string $message;
    private array $context;

    public function __construct()
    {
        parent::__construct($this);
    }

    public function log($level, Stringable|string $message, array $context = []): void
    {
        $this->level = $level;
        $this->message = $message;
        $this->context = $context;
    }

    public function assertMessageLogged($level, $message, array $context = []): void
    {
        Assert::assertEquals($level, $this->level);
        Assert::assertEquals($message, $this->message);
        Assert::assertEquals($context, $this->context);
    }
}