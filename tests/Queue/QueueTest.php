<?php declare(strict_types=1);

namespace Test\Fittinq\Symfony\RabbitMQ\Queue;

use Exception;
use Fittinq\Symfony\RabbitMQ\RabbitMQ\HeaderBag;
use PhpAmqpLib\Exception\AMQPIOWaitException;
use PHPUnit\Framework\TestCase;
use Psr\Log\LogLevel;
use stdClass;
use Test\Fittinq\Symfony\RabbitMQ\Configuration;
use Throwable;

class QueueTest extends TestCase
{
    private Configuration $configuration;
    private AbstractLoggerMock $logger;

    protected function setUp(): void
    {
        parent::setUp();
        $this->configuration = new Configuration();
        $this->logger = $this->configuration->getLogger();
    }

    public function test_queuesShouldBeDurable(): void
    {
        $queue = $this->configuration->getRabbitMQ()->getQueue('bananas');
        $queue->declare();

        $this->configuration->getChannel()->expectQueueToBeDurable('bananas');
    }

    public function test_queuesShouldBeOfTypeQuorum(): void
    {
        $queue = $this->configuration->getRabbitMQ()->getQueue('bananas');
        $queue->declare();

        $this->configuration->getChannel()->expectQueueToBeOfTypeQuorum('bananas');
    }

    /**
     * @throws Throwable
     */
    public function test_acknowledgeMessageIfHandlerIsExecutedSuccessfully(): void
    {
        $queue = $this->configuration->getRabbitMQ()->getQueue('source.event.target');
        $message = $this->configuration->getChannel()->sendMessageToQueue(new HeaderBag(), 'source.event.target', new stdClass(), '', 'source.event.target');

        $queue->consume($this->configuration->getHandlerMock());

        $message->expectMessageToBeAcknowledged();
    }

    /**
     * @throws Throwable
     */
    public function test_deadLetterMessageIfHandlerFails(): void
    {
        $queue = $this->configuration->getRabbitMQ()->getQueue('bananas');
        $message = $this->configuration->getChannel()->sendMessageToQueue(new HeaderBag(), 'bananas', new stdClass());
        $this->configuration->getHandlerMock()->setThrowException(new Exception('make_it_fail'));

        $queue->consume($this->configuration->getHandlerMock());

        $message->expectMessageToBeDeadLettered();
    }

    /**
     * @throws Throwable
     */
    public function test_logMessageIfHandlerFails(): void
    {
        $queue = $this->configuration->getRabbitMQ()->getQueue('source.event.target');
        $context = [
            'message_id' => '66c2fe0a298f1',
            'application_headers' => null,
            'timestamp' => 1724055050,
            'queue' => 'source.event.target'
        ];

        $this->configuration->getChannel()->sendMessageToQueue(new HeaderBag($context), 'source.event.target', new stdClass(), '', 'source.event.target');
        $this->configuration->getHandlerMock()->setThrowException(new Exception('make_it_fail'));

        $queue->consume($this->configuration->getHandlerMock());

        $this->logger->assertMessageLogged(LogLevel::WARNING, "Exception: make_it_fail: /opt/tests/Queue/QueueTest.php: 83", $context);
    }

    /**
     * @throws Throwable
     */
    public function test_rethrowExceptionIfChannelFails(): void
    {
        $queue = $this->configuration->getRabbitMQ()->getQueue('bananas');
        $this->configuration->getChannel()->sendMessageToQueue(new HeaderBag(), 'bananas', new stdClass());
        $this->configuration->getChannel()->setException(new AMQPIOWaitException());
        $this->configuration->getHandlerMock()->setThrowException(new Exception('make_it_fail'));


        $this->expectException(AMQPIOWaitException::class);
        $queue->consume($this->configuration->getHandlerMock());
    }

    public function test_deleteQueueIfRequested(): void
    {
        $queue = $this->configuration->getRabbitMQ()->getQueue('bananas');
        $queue->declare();

        $queue->delete();

        $this->configuration->getChannel()->expectQueueToBeDeleted('bananas');
    }
}
