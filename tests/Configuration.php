<?php declare(strict_types=1);

namespace Test\Fittinq\Symfony\RabbitMQ;

use Fittinq\Symfony\RabbitMQ\ErrorHandler\ErrorHandler;
use Fittinq\Symfony\RabbitMQ\Handler\Handler;
use Fittinq\Symfony\RabbitMQ\RabbitMQ\RabbitMQ;
use Test\Fittinq\Symfony\RabbitMQ\Consumer\ErrorHandlerMock;
use Test\Fittinq\Symfony\RabbitMQ\Consumer\HandlerMock;
use Test\Fittinq\Symfony\RabbitMQ\Queue\AbstractLoggerMock;
use Test\Fittinq\Symfony\RabbitMQ\RabbitMQ\AMQPChannelMock;
use Test\Fittinq\Symfony\RabbitMQ\RabbitMQ\AMQPLazyConnectionMock;

class Configuration
{
    private AMQPChannelMock $channel;
    private AMQPLazyConnectionMock $connection;
    private RabbitMQ $rabbitmq;
    private Handler $handlerMock;
    private ErrorHandler $errorHandlerMock;
    private AbstractLoggerMock $logger;

    public function __construct()
    {
        $this->channel = new AMQPChannelMock();
        $this->connection = new AMQPLazyConnectionMock($this->channel);
        $this->rabbitmq = new RabbitMQ($this->connection);

        $this->logger = new AbstractLoggerMock();
        $this->handlerMock = new HandlerMock('source.event.target', $this->logger);
        $this->errorHandlerMock = new ErrorHandlerMock();
    }

    public function getLogger(): AbstractLoggerMock
    {
        return $this->logger;
    }

    public function getChannel(): AMQPChannelMock
    {
        return $this->channel;
    }

    public function getConnection(): AMQPLazyConnectionMock
    {
        return $this->connection;
    }

    public function getRabbitmq(): RabbitMQ
    {
        return $this->rabbitmq;
    }

    public function getHandlerMock(): HandlerMock
    {
        return $this->handlerMock;
    }

    public function getErrorHandlerMock(): ErrorHandlerMock
    {
        return $this->errorHandlerMock;
    }
}
