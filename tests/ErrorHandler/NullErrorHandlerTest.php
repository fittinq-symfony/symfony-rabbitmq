<?php declare(strict_types=1);

namespace Test\Fittinq\Symfony\RabbitMQ\ErrorHandler;

use Fittinq\Symfony\RabbitMQ\ErrorHandler\NullErrorHandler;
use PhpAmqpLib\Exception\AMQPIOException;
use PHPUnit\Framework\TestCase;

class NullErrorHandlerTest extends TestCase
{
    public function test_nullErrorHandlerTest(): void
    {
        $nullErrorHandler = new NullErrorHandler();
        $nullErrorHandler->handle(new AMQPIOException('failure'), 'source.event.target');

        $this->assertTrue(true);
    }
}
