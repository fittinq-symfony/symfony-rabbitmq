<?php declare(strict_types=1);

namespace Test\Fittinq\Symfony\RabbitMQ\Connection;

use Fittinq\Symfony\RabbitMQ\RabbitMQ\HeaderBag;
use Test\Fittinq\Symfony\RabbitMQ\Configuration;
use PHPUnit\Framework\TestCase;
use Exception;
use stdClass;

class ConnectionTest extends TestCase
{
    private Configuration $configuration;

    protected function setUp(): void
    {
        parent::setUp();
        $this->configuration = new Configuration();
    }

    /**
     * Try to keep the connection/channel count low. Use separate connections to publish and consume. Ideally, you
     * should have one connection per process, and then use one channel per thread in your application.
     *
     *      Reuse connections
     *      1 connection for publishing
     *      1 connection for consuming
     */
    public function test_exchangesAlwaysUseOneChannel(): void
    {
        $this->configuration->getRabbitmq()->getExchange('bananas');
        $this->configuration->getRabbitmq()->getExchange('apples');
        $this->configuration->getRabbitmq()->getExchange('strawberries');

        $this->configuration->getConnection()->expectOnlyOneChannelToBeOpened();
    }

    public function test_queuesAlwaysUseOneChannel(): void
    {
        $this->configuration->getRabbitmq()->getQueue('bananas');
        $this->configuration->getRabbitmq()->getQueue('apples');
        $this->configuration->getRabbitmq()->getQueue('strawberries');

        $this->configuration->getConnection()->expectOnlyOneChannelToBeOpened();
    }

   /**
     * @throws Exception
     */
    public function test_connectionShouldClosedIfMessageLimitIsReached(): void
    {
        $messageLimit = 5;

        $queue = $this->configuration->getRabbitmq()->getQueue('bananas');

        for ($i = 0; $i < $messageLimit + 5; $i++) {
            $this->configuration->getChannel()->sendMessageToQueue(new HeaderBag(), 'bananas', new stdClass());
        }

        $queue->consume($this->configuration->getHandlerMock(), $messageLimit);
        $this->configuration->getConnection()->expectToBeClosed();
        $this->configuration->getChannel()->hasMessagesInQueue(5, 'bananas');
    }
}

